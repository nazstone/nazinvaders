import React, {useState} from 'react';

import {View} from 'react-native';
import {text} from '../utils/font';
import Text from './components/Text';
import {useNavigation} from '@react-navigation/native';
import {getAllInvaderFromSpotter} from '../service/fetch.data';

const margin = {
  margin: 14,
};
const marginAndText = {
  ...margin,
  ...text,
};

const getFakeData = async (progress: (percent: number) => void) => {
  await new Promise(resolve => setTimeout(resolve, 500));
  progress(10);
  await new Promise(resolve => setTimeout(resolve, 500));
  progress(40);
  await new Promise(resolve => setTimeout(resolve, 500));
  progress(50);
  await new Promise(resolve => setTimeout(resolve, 500));
  progress(70);
  await new Promise(resolve => setTimeout(resolve, 500));
  progress(100);
};

export default function LoadingView() {
  const navigation = useNavigation<any>();

  const [downloading, setDownloading] = useState<Boolean>(false);
  const [percent, setPercent] = useState<number>(0);

  console.log('loading view', percent, 'downaloding', downloading);

  if (!downloading) {
    // no data, so download it
    setDownloading(true);

    console.log('fetching data');
    getAllInvaderFromSpotter((p: number) => {
      setPercent(Math.floor(p));
    }).then(() => {
      console.log('end downloading data');
      navigation.goBack();
    });
  }

  return (
    <View style={margin}>
      <Text style={marginAndText}>Loading invaders data from website</Text>
      <Text style={marginAndText}>Please wait a few seconds</Text>
      <Text style={marginAndText}>{percent}% downloaded</Text>
    </View>
  );
}
