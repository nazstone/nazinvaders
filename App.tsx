import React, {useState} from 'react';
import {Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {fontFamily, theme} from './utils/style';
import List from './views/List';
import Map from './views/Map';
import About from './views/About';
import Detail from './views/Detail';
import City from './views/City';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import prefReduce from './reducer/pref.reduce';

import LoadingView from './views/Loading';
import {init} from './repo/db';

const Stack = createStackNavigator();
const store = createStore(prefReduce);

const style = {
  height: 50,
  width: 50,
};

init();

function App(): React.JSX.Element {
  return (
    <Provider store={store}>
      <NavigationContainer theme={theme}>
        <Stack.Navigator
          initialRouteName="List"
          screenOptions={{
            headerTintColor: 'white',
            headerTitleStyle: {
              ...fontFamily,
            },
          }}>
          <Stack.Group>
            <Stack.Screen
              name="List"
              component={List}
              options={({navigation}) => ({
                title: 'Invaders',
                headerRight: () => (
                  <Image
                    style={style}
                    resizeMode="center"
                    onTouchEnd={() => {
                      navigation.navigate('Map');
                    }}
                    source={require('./assets/img/map.png')}
                  />
                ),
              })}
            />
            <Stack.Screen
              name="Detail"
              component={Detail}
              options={({route}) => ({
                title: route.params?.item.name || 'Detail',
              })}
            />
            <Stack.Screen
              name="CityList"
              component={City}
              options={{
                title: 'List of cities',
              }}
            />
            <Stack.Screen
              name="Map"
              component={Map}
              options={{
                title: 'Map',
                headerTransparent: true,
              }}
            />
            <Stack.Screen
              name="Help"
              component={About}
              options={{
                title: 'Help',
              }}
            />
          </Stack.Group>
          <Stack.Group screenOptions={{presentation: 'modal'}}>
            <Stack.Screen
              name="LoadingView"
              options={() => ({
                title: 'Loading data',
                headerShown: false,
              })}
              component={LoadingView}></Stack.Screen>
          </Stack.Group>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
