import axios from 'axios';
import {HTMLElement, parse} from 'node-html-parser';
import {decode} from 'html-entities';
import {InvaderData} from './invader.type';
import {insertAll} from '../repo/db';

const fetchMainPage = async (page: number = 1) => {
  const formdata = new FormData();
  formdata.append('toutlieu', 'on');
  formdata.append('SPACE', 'on');
  formdata.append('BRL', 'on');
  formdata.append('FKF', 'on');
  formdata.append('KLN', 'on');
  formdata.append('MUN', 'on');
  formdata.append('MLB', 'on');
  formdata.append('PRT', 'on');
  formdata.append('WN', 'on');
  formdata.append('DHK', 'on');
  formdata.append('ANVR', 'on');
  formdata.append('BXL', 'on');
  formdata.append('CHAR', 'on');
  formdata.append('RDU', 'on');
  formdata.append('BT', 'on');
  formdata.append('POTI', 'on');
  formdata.append('GRU', 'on');
  formdata.append('SP', 'on');
  formdata.append('HK', 'on');
  formdata.append('DJN', 'on');
  formdata.append('BRC', 'on');
  formdata.append('BBO', 'on');
  formdata.append('MLGA', 'on');
  formdata.append('MEN', 'on');
  formdata.append('LA', 'on');
  formdata.append('MIA', 'on');
  formdata.append('NY', 'on');
  formdata.append('SD', 'on');
  formdata.append('AIX', 'on');
  formdata.append('AVI', 'on');
  formdata.append('BTA', 'on');
  formdata.append('CAPF', 'on');
  formdata.append('CLR', 'on');
  formdata.append('CON', 'on');
  formdata.append('CAZ', 'on');
  formdata.append('DIJ', 'on');
  formdata.append('FTBL', 'on');
  formdata.append('FRQ', 'on');
  formdata.append('GRN', 'on');
  formdata.append('LCT', 'on');
  formdata.append('REUN', 'on');
  formdata.append('LIL', 'on');
  formdata.append('LBR', 'on');
  formdata.append('LY', 'on');
  formdata.append('MARS', 'on');
  formdata.append('MTB', 'on');
  formdata.append('MPL', 'on');
  formdata.append('NA', 'on');
  formdata.append('NIM', 'on');
  formdata.append('PAU', 'on');
  formdata.append('PRP', 'on');
  formdata.append('RN', 'on');
  formdata.append('TLS', 'on');
  formdata.append('VLMO', 'on');
  formdata.append('VRS', 'on');
  formdata.append('LDN', 'on');
  formdata.append('AN', 'on');
  formdata.append('NCL', 'on');
  formdata.append('VRN', 'on');
  formdata.append('ELT', 'on');
  formdata.append('RA', 'on');
  formdata.append('ROM', 'on');
  formdata.append('TK', 'on');
  formdata.append('MBSA', 'on');
  formdata.append('MRAK', 'on');
  formdata.append('RBA', 'on');
  formdata.append('CCU', 'on');
  formdata.append('KAT', 'on');
  formdata.append('AMS', 'on');
  formdata.append('NOO', 'on');
  formdata.append('RTD', 'on');
  formdata.append('FAO', 'on');
  formdata.append('LJU', 'on');
  formdata.append('HALM', 'on');
  formdata.append('VSB', 'on');
  formdata.append('ANZR', 'on');
  formdata.append('BSL', 'on');
  formdata.append('BRN', 'on');
  formdata.append('GNV', 'on');
  formdata.append('LSN', 'on');
  formdata.append('GRTI', 'on');
  formdata.append('BGK', 'on');
  formdata.append('DJBA', 'on');
  formdata.append('IST', 'on');
  formdata.append('prs', 'on');
  formdata.append('toutparis', 'on');
  formdata.append('PA01', 'on');
  formdata.append('PA02', 'on');
  formdata.append('PA03', 'on');
  formdata.append('PA04', 'on');
  formdata.append('PA05', 'on');
  formdata.append('PA06', 'on');
  formdata.append('PA07', 'on');
  formdata.append('PA08', 'on');
  formdata.append('PA09', 'on');
  formdata.append('PA10', 'on');
  formdata.append('PA11', 'on');
  formdata.append('PA12', 'on');
  formdata.append('PA13', 'on');
  formdata.append('PA14', 'on');
  formdata.append('PA15', 'on');
  formdata.append('PA16', 'on');
  formdata.append('PA17', 'on');
  formdata.append('PA18', 'on');
  formdata.append('PA19', 'on');
  formdata.append('PA20', 'on');
  formdata.append('PA77', 'on');
  formdata.append('PA92', 'on');
  formdata.append('PA93', 'on');
  formdata.append('PA94', 'on');
  formdata.append('PA95', 'on');
  formdata.append('numero', '');
  formdata.append('choixmotif', 'ou');
  formdata.append('choixfond', 'ou');
  formdata.append('couleurmotif', '0000000000000000000');
  formdata.append('couleurfond', '0000000000000000000');
  formdata.append('x', '74');
  formdata.append('y', '19');
  formdata.append('page', page);
  const response = await axios.post(
    'https://www.invader-spotter.art/listing.php',
    formdata,
    {
      headers: {
        'Content-Type': 'multipart/form-data',
        Referer: 'https://www.invader-spotter.art/cherche.php',
      },
    },
  );

  return response.data;
};

const fillInvaderDataGlobalInfo = (
  invaderData: InvaderData,
  root: HTMLElement,
  index: number,
) => {
  const globalInfo = root.querySelector(
    `tr.haut:nth-child(${index * 3 + 1}) > td > font`,
  ).outerHTML;
  const resGlobalInfo = parse(globalInfo);
  const b = resGlobalInfo.querySelector('b').outerHTML;
  const regexMatch = b.match(/<b>(?<name>.*) \[(?<points>.*) pts\]/);
  if (regexMatch?.groups) {
    invaderData.name = regexMatch.groups.name;
    invaderData.points = regexMatch.groups.points;
    invaderData.city =
      decode(resGlobalInfo.querySelector('a')?.innerHTML) || '';
  }

  const indexOfDateSource = globalInfo.indexOf('<br>Date et source : ');
  const dateSource = globalInfo.substring(indexOfDateSource);
  invaderData.date = dateSource
    .substring('<br>Date et source : '.length, dateSource.indexOf(' ('))
    .replace(/\n\s+/, ' ')
    .trim();

  const statusTmp = globalInfo.substring(0, indexOfDateSource);
  invaderData.status = decode(
    statusTmp
      .substring(statusTmp.lastIndexOf('>') + 1)
      .replace(/\n\s+/, ' ')
      .trim(),
  );
};

const fillInvaderDataComment = (
  invaderData: InvaderData,
  root: HTMLElement,
  index: number,
) => {
  const commentsDiv = root.querySelectorAll(`#ajoutform${index} div`);

  invaderData.comments = [];
  for (let i = 0; i < commentsDiv.length; i += 1) {
    const resComments = commentsDiv[i].innerHTML.match(
      /<b><u>(.*)<\/u><\/b> \(([0-9]{2}\/[0-9]{2}\/[0-9]{4})\) :<br>(.*)<br>/,
    );

    if (resComments) {
      const author = resComments[1];
      const date = resComments[2];
      const comment = decode(resComments[3]);
      invaderData.comments.push({
        author,
        date,
        comment,
      });
    }
  }
};

const fillInvaderDataImg = (
  invaderData: InvaderData,
  root: HTMLElement,
  index: number,
  name: string,
  position: number,
) => {
  const imgMain = root.querySelector(
    `tr.haut:nth-child(${index * 3 + 1}) > td:nth-child(${position}) img`,
  );
  if (imgMain && imgMain.attributes['src']) {
    const imgSrc = `https://www.invader-spotter.art/${imgMain.attributes['src']}`;
    invaderData.image = {
      ...invaderData.image,
      [name]: imgSrc,
    };
  }
};

let indexGlobal = 0;

/**
 *
 * @param {HTMLElement} root
 * @param {Number} index
 * @returns
 */
const getInfoMetadata = (root: HTMLElement, index: number) => {
  const invaderData: InvaderData = {id: indexGlobal++};

  fillInvaderDataGlobalInfo(invaderData, root, index);
  fillInvaderDataComment(invaderData, root, index);
  fillInvaderDataImg(invaderData, root, index, 'main', 1);
  fillInvaderDataImg(invaderData, root, index, 'street', 2);
  fillInvaderDataImg(invaderData, root, index, 'streetUpdated', 3);

  return invaderData;
};

const getMetaDataPerPage = (root: HTMLElement) => {
  const invaderDatas: Array<InvaderData> = [];
  const length = root.querySelectorAll('table tr.haut').length;
  for (let i = 0; i < length; i += 1) {
    try {
      invaderDatas.push(getInfoMetadata(root, i));
    } catch (error) {
      console.error('Parsing data error', error, i);
    }
  }

  return invaderDatas;
};

const nextPage = async (page?: number) => {
  const resp = await fetchMainPage(page);
  const root = parse(resp);
  const invaders = getMetaDataPerPage(root);
  await insertAll(invaders);
  return resp;
};

export const getAllInvaderFromSpotter = async (
  progress: (percent: number) => void,
) => {
  const resp = await nextPage();
  const root = parse(resp);

  const as = root.querySelectorAll('div#contenu p a');

  const pages = new Set(
    as
      .filter(e => e.attributes['href'].startsWith('javascript:changepage'))
      .map(e => e.textContent),
  );

  let index = 0;

  const countPages = Array.from(pages).length + 1;
  // console.log('pages number', countPages);

  for (const e of Array.from(pages)) {
    progress((index / countPages) * 100);
    await nextPage(Number(e));
    index += 1;
  }
};
