export type InvaderData = {
  id: number;
  name: string;
  points: string;
  city: string;
  date: string;
  status: string;
  comments: Array<Comment>;
  image: Image;
};

export type Comment = {
  author: string;
  date: string;
  comment: string;
};

export type Image = {
  main: string;
  street: string;
  streetUpdated: string;
};
