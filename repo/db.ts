import SQLite, {SQLiteDatabase} from 'react-native-sqlite-storage';
import {Comment, InvaderData} from '../service/invader.type';
SQLite.enablePromise(true);
SQLite.DEBUG(false);

let db: SQLiteDatabase | null = null;

const init = async () => {
  db = await SQLite.openDatabase({
    name: 'invaders.sqlite3',
    location: 'default',
  });

  await db.executeSql(`
      CREATE TABLE IF NOT EXISTS item (
        id INTEGER PRIMARY KEY,
        name TEXT,
        points INTEGER,
        city TEXT,
        status TEXT,
        date TEXT,
        image_main TEXT,
        image_street TEXT,
        image_street_updated TEXT,
        pin TEXT
      );
    `);
  await db.executeSql(`
      CREATE TABLE IF NOT EXISTS item_comment (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        author TEXT,
        date TEXT,
        comment TEXT,
        itemid NUMBER,
        FOREIGN KEY(itemid) REFERENCES item(id)
      );
    `);
  await db.executeSql(`
      CREATE TABLE IF NOT EXISTS pref_config (
        city text
      );
    `);
};

const insertAll = async (invaders: Array<InvaderData>) => {
  await db?.transaction(async tx => {
    const [_tx, res] = await tx.executeSql(
      `insert into item (id, name, points, city, status, date, image_main, image_street, image_street_updated) values ${invaders
        .map(e => `(?, ?, ?, ?, ?, ?, ?, ?, ?)`)
        .join(',')}`,
      invaders
        .map(invaderTmp => [
          invaderTmp.id,
          invaderTmp.name,
          invaderTmp.points,
          invaderTmp.city,
          invaderTmp.status,
          invaderTmp.date,
          invaderTmp.image.main,
          invaderTmp.image.street,
          invaderTmp.image.streetUpdated,
        ])
        .reduce((acc, cur) => [...acc, ...cur], []),
    );
  });

  const queryInsertCommentsVal: Array<{id: number} & Comment> = [];
  for (const invaderTmp of invaders) {
    for (const comment of invaderTmp.comments) {
      queryInsertCommentsVal.push({
        id: invaderTmp.id,
        author: comment.author,
        date: comment.date,
        comment: comment.comment,
      });
    }
  }

  const maxSqliteParam = 999;
  const numberParam = 4;
  let maxLoop = Math.ceil(
    (queryInsertCommentsVal.length * numberParam) / maxSqliteParam,
  );

  for (let i = 0; i < maxLoop; i += 1) {
    const subQuery = queryInsertCommentsVal.splice(0, maxSqliteParam / 4);

    let param = '';
    const val: any[] | undefined = [];

    for (let commentTmp of subQuery) {
      if (param) {
        param = `${param}, (?, ? , ?, ?)`;
      } else {
        param = '(?, ? , ?, ?)';
      }
      val.push(
        commentTmp.id,
        commentTmp.author,
        commentTmp.date,
        commentTmp.comment,
      );
    }
    const query = `insert into item_comment (itemid, author, date, comment) values ${param}`;
    await db?.transaction(async tx => {
      const [_tx, res] = await tx.executeSql(query, val);
    });
  }
};

const getPref = async () => {
  return await exec('select * from pref_config');
};

const setPref = async ({city}) => {
  const pref = await getPref();

  const query =
    pref.length > 0
      ? 'update pref_config set city = ?'
      : 'insert into pref_config (city) values (?)';

  await exec(query, [city]);
};

const getItemCount = async ({city}: {city: string | null}) => {
  let where = 'where 1=1';
  if (city) {
    where += ' and city = ?';
  }
  const valueArray = city ? [city] : [];
  const res = await exec(
    `SELECT count(1) as "counter" FROM item ${where}`,
    valueArray,
  );
  return (res.item && res.item(0).counter) || 0;
};

const getItemPaginate = async (
  {city}: {city: string},
  {limit, offset}: {limit: number; offset: number},
) => {
  let where = 'where 1=1';
  if (city) {
    where += ' and city = ?';
  }
  const valueArray = city ? [city] : [];
  const res = await exec(
    `SELECT * FROM item ${where} order by name limit ${limit} offset ${offset}`,
    valueArray,
  );
  return res;
};

const getCities = async () => {
  const res = await exec(
    'SELECT city as name, count(1) as "count" FROM item group by city',
  );
  return res;
};

const getComments = async itemid => {
  const res = await exec(
    `
    SELECT rowid as id, comment, date, author 
    FROM item_comment 
    where itemid = ? `,
    [itemid],
  );
  return res;
};
const getItem = async itemid => {
  const res = await exec(
    `
    SELECT * 
    FROM item 
    where rowid = ?`,
    [itemid],
  );
  return res;
};

const savePin = async item => {
  await exec('update item set pin = ? where id = ?', [item.pin, item.id]);
};

const setItemFounded = async item => {
  await exec('update item set founded = ? where id = ?', [
    item.founded,
    item.id,
  ]);
};

const exec = async (query: string, args: Array<any> = []) => {
  try {
    const [r] = await db!.executeSql(query, args);
    return r.rows || undefined;
  } catch (error) {
    console.error('db exec error', error);
  }
};

export {
  init,
  getItem,
  getItemCount,
  getItemPaginate,
  getCities,
  getPref,
  setPref,
  getComments,
  savePin,
  setItemFounded,
  insertAll,
};
