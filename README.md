# Dev

## Info

Install ̀`Node 20` and `Android Studio`.
For `Android Studio` don't forget to launch it (`Unsupported class file major version 61`) and install standard lib.
Follow react-native install [guide](https://reactnative.dev/docs/environment-setup?guide=native)

## Install

` ̀``shell
yarn
 `̀``

## DB

A sqlite db must be fill by information available from InvaderSpotterExtract tools (target => android/app/src/main/assets/www)

## Dev tools

```shell
npm run start
```

## deploy debug version

```shell
npm run android
```

# Build

## Clean

```shell
cd android && ./gradlew clean && cd ..
```

## APK

```shell
cd android && ./gradlew assembleRelease
```

output will be in `android/app/build/outputs/apk`

## deploy on device

device must be connected

```shell
npx react-native run-android --variant=release
```

## undeploy

`adb uninstall com.nazinvader`

## Emulator

List: `$ANDROID_HOME/emulator/emulator -list-avds`
Run specific android: `$ANDROID_HOME/emulator/emulator -avd Pixel_3a_API_34_extension_level_7_x86_64`
Read SQLITE on android shell `sqlite3 /data/data/com.invaders/databases/invaders.sqlite3 "SELECT name FROM sqlite_master WHERE type='table'"`
